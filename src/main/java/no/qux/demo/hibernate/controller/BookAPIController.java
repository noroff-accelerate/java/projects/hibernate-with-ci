package no.qux.demo.hibernate.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.hibernate.model.CommonResponse;
import no.qux.demo.hibernate.model.dbo.Book;
import no.qux.demo.hibernate.model.dbo.Genre;
import no.qux.demo.hibernate.repository.BookRepository;
import no.qux.demo.hibernate.repository.GenreRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@Tag(name = "Book")
@RequestMapping("/api/v0/book")
public class BookAPIController {
    private final BookRepository bookRepo;
    private final GenreRepository genreRepo;

    public BookAPIController(
            BookRepository bookRepo,
            GenreRepository genreRepo
    ) {
        this.bookRepo = bookRepo;
        this.genreRepo = genreRepo;
    }

    @GetMapping("/")
    public ResponseEntity<CommonResponse<Collection<Book>>> getAll(
            @RequestParam(required = false) Optional<String> title,
            @RequestParam(required = false, defaultValue = "1") int limit,
            @RequestParam(required = false, defaultValue = "1") int page
    ) {
        Collection<Book> books = bookRepo.findAll();

        return ResponseEntity
                .ok()
                .body(new CommonResponse<>(books));
    }

    @DeleteMapping("/genre/{id}")
    public String deleteGenre(
            @PathVariable long id
    ) {
//        Optional<Genre> genre = genreRepo.findById(id);
//        if (genre.isPresent()) {
//            for (Book book : genre.get().getBooks()) {
//                book.getGenre().remove(genre);
//                bookRepo.save(book);
//            }
//
//        }
        genreRepo.deleteById(id);
        return "foo";
    }
}
