package no.qux.demo.hibernate.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.hibernate.model.CommonResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Root")
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
public class RootAPIController {
    @GetMapping("/")
    public ResponseEntity<CommonResponse> getApiInfo() {
        return ResponseEntity
                .ok()
                .body(new CommonResponse(-1, "No API info yet"));
    }

    @GetMapping("/inspect")
    @PreAuthorize("hasAuthority('GROUP_admin')")
    public ResponseEntity<CommonResponse<Object>> inspect(
            @AuthenticationPrincipal Jwt principal
    ) {
        return ResponseEntity
                .ok(new CommonResponse<>(principal.getClaims().get("sub")));
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('hibernate-admin')")
    public ResponseEntity<CommonResponse<String>> adminProtected() {
        return ResponseEntity.ok(new CommonResponse<>("Hello admin"));
    }
}
