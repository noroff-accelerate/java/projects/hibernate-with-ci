package no.qux.demo.hibernate.model.dbo;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @NaturalId
    @JsonIgnore
    @Column(name = "external_id")
    private String externalId;

    @Column(nullable = false)
    private String title;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String series;

    @JsonGetter("authors")
    public List<String> authors() {
        return authors.stream()
                .map(author -> String.format("/author/%d", author.getAuthor().getId()))
                .collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "book")
    private List<AuthorContribution> authors;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private float rating;

    @Column(columnDefinition = "TEXT")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String language;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String isbn;

    @ManyToMany
    @JoinTable(name = "book_genre")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Genre> genre;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bookFormat;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String edition;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int pages;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String publisher;

    @Column
    @Temporal(TemporalType.DATE)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date publishDate;

    @Column
    @Temporal(TemporalType.DATE)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date firstPublishDate;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int numRatings;

    // TODO Needs JsonGetter
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "countOneStar", column = @Column(name = "stars_1")),
            @AttributeOverride(name = "countTwoStar", column = @Column(name = "stars_2")),
            @AttributeOverride(name = "countThreeStar", column = @Column(name = "stars_3")),
            @AttributeOverride(name = "countFourStar", column = @Column(name = "stars_4")),
            @AttributeOverride(name = "countFiveStar", column = @Column(name = "stars_5")),
    })
    private Stars stars;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private float likedPercent;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String coverImg;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int score;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int votes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public List<AuthorContribution> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorContribution> authors) {
        this.authors = authors;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    public String getBookFormat() {
        return bookFormat;
    }

    public void setBookFormat(String bookFormat) {
        this.bookFormat = bookFormat;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Date getFirstPublishDate() {
        return firstPublishDate;
    }

    public void setFirstPublishDate(Date firstPublishDate) {
        this.firstPublishDate = firstPublishDate;
    }

    public int getNumRatings() {
        return numRatings;
    }

    public void setNumRatings(int numRatings) {
        this.numRatings = numRatings;
    }

    public Stars getStars() {
        return stars;
    }

    public void setStars(Stars stars) {
        this.stars = stars;
    }

    public float getLikedPercent() {
        return likedPercent;
    }

    public void setLikedPercent(float likedPercent) {
        this.likedPercent = likedPercent;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }
}
