package no.qux.demo.hibernate.model.dbo;

import javax.persistence.Embeddable;

@Embeddable
public class Stars {
    private int countOneStar;
    private int countTwoStar;
    private int countThreeStar;
    private int countFourStar;
    private int countFiveStar;

    public Stars(int ...args) {
        if (args.length != 5) {
            throw new IllegalArgumentException("Stars requires five values");
        }

        this.countOneStar = args[0];
        this.countTwoStar = args[1];
        this.countThreeStar = args[2];
        this.countFourStar = args[3];
        this.countFiveStar = args[4];
    }

    public Stars() {}

    public int getCountOneStar() {
        return countOneStar;
    }

    public void setCountOneStar(int countOneStar) {
        this.countOneStar = countOneStar;
    }

    public int getCountTwoStar() {
        return countTwoStar;
    }

    public void setCountTwoStar(int countTwoStar) {
        this.countTwoStar = countTwoStar;
    }

    public int getCountThreeStar() {
        return countThreeStar;
    }

    public void setCountThreeStar(int countThreeStar) {
        this.countThreeStar = countThreeStar;
    }

    public int getCountFourStar() {
        return countFourStar;
    }

    public void setCountFourStar(int countFourStar) {
        this.countFourStar = countFourStar;
    }

    public int getCountFiveStar() {
        return countFiveStar;
    }

    public void setCountFiveStar(int countFiveStar) {
        this.countFiveStar = countFiveStar;
    }
}
