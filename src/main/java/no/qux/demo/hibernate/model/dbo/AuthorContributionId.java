package no.qux.demo.hibernate.model.dbo;

import java.io.Serializable;
import java.util.Objects;

public class AuthorContributionId implements Serializable {
    private long book;
    private long author;

    public AuthorContributionId() {}

    public AuthorContributionId(long book, long author) {
        this.book = book;
        this.author = author;
    }

    public long getBook() {
        return book;
    }

    public void setBook(long book) {
        this.book = book;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthorContributionId)) return false;
        AuthorContributionId that = (AuthorContributionId) o;
        return getBook() == that.getBook() && getAuthor() == that.getAuthor();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBook(), getAuthor());
    }
}
