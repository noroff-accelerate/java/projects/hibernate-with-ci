package no.qux.demo.hibernate.model.dbo;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
@IdClass(AuthorContributionId.class)
public class AuthorContribution {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private Book book;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private Author author;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String contribution;

    public AuthorContribution(Book book, Author author, String contribution) {
        this.book = book;
        this.author = author;
        this.contribution = contribution;
    }

    public AuthorContribution() {}

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getContribution() {
        return contribution;
    }

    public void setContribution(String contribution) {
        this.contribution = contribution;
    }
}
