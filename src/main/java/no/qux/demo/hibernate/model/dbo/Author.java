package no.qux.demo.hibernate.model.dbo;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToMany(mappedBy = "author")
    private List<AuthorContribution> books;

    @Column(nullable = false)
    private String name;

    public Author() {}

    public Author(long id, List<AuthorContribution> books, String name) {
        this.id = id;
        this.books = books;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<AuthorContribution> getBooks() {
        return books;
    }

    public void setBooks(List<AuthorContribution> books) {
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
