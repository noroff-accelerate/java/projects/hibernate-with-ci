package no.qux.demo.hibernate.service;

public interface DatabaseSeedService {
    long process();
}
