package no.qux.demo.hibernate.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.qux.demo.hibernate.model.dbo.*;
import no.qux.demo.hibernate.repository.AuthorContributionRepository;
import no.qux.demo.hibernate.repository.AuthorRepository;
import no.qux.demo.hibernate.repository.BookRepository;
import no.qux.demo.hibernate.repository.GenreRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DatabaseSeedServiceImpl implements DatabaseSeedService {
    private final Pattern authorPattern = Pattern.compile("(?<name>[^(]+)(\\((?<contrib>[A-Za-z0-9/ ]+)\\))?.*");
    private final Logger logger = LoggerFactory.getLogger(DatabaseSeedServiceImpl.class);
    private final BookRepository bookRepo;
    private final AuthorRepository authorRepo;
    private final GenreRepository genreRepo;
    private final AuthorContributionRepository authorContribRepo;
    private final ObjectMapper objectMapper;

    public DatabaseSeedServiceImpl(
        BookRepository bookRepo,
        AuthorRepository authorRepo,
        GenreRepository genreRepo,
        AuthorContributionRepository authorContribRepo,
        ObjectMapper objectMapper
    ) {
        this.bookRepo = bookRepo;
        this.authorRepo = authorRepo;
        this.genreRepo = genreRepo;
        this.authorContribRepo = authorContribRepo;
        this.objectMapper = objectMapper;
    }

    @EventListener
    public void setupSeedData(ContextRefreshedEvent ctxRefreshed) {
        logger.info("Added {} records to the database", this.process());
    }

    public long process() {
        logger.debug("Starting database seed");

        try (InputStream is = getClass().getClassLoader().getResourceAsStream("data/books.csv")) {
            if (is == null) {
                logger.error("Failed to find data file. Database seed failed");
                return 0L;
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            List<String> firstLine = Arrays.stream(reader.readLine().split(",")).map(name -> name.substring(1, name.length() - 1)).toList();
            CSVParser csv = new CSVParser(
                    reader,
                    CSVFormat.Builder
                            .create(CSVFormat.DEFAULT)
                            .setSkipHeaderRecord(true)
                            .setHeader(firstLine.toArray(new String[firstLine.size()]))
                            .build()
            );

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            int count = 0;
            for (CSVRecord record : csv.getRecords()) {
                if (count == 10) {
                    break;
                }

                Book book = new Book();
                book.setExternalId(record.get("bookId"));
                book.setTitle(record.get("title"));
                book.setSeries(record.get("series"));
                book.setDescription(record.get("description"));
                book.setLanguage(record.get("language"));
                book.setIsbn(record.get("isbn"));
                book.setBookFormat(record.get("bookFormat"));
                book.setEdition(record.get("edition"));
                book.setPublisher(record.get("publisher"));
                book.setCoverImg(record.get("coverImg"));

                try {
                    book.setScore(Integer.parseInt(record.get("bbeScore")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setVotes(Integer.parseInt(record.get("bbeVotes")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setPages(record.get("pages").isEmpty() ? 0 : Integer.parseInt(record.get("pages")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setRating(record.get("rating").isEmpty() ? 0f : Float.parseFloat(record.get("rating")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setNumRatings(record.get("numRatings").isEmpty() ? 0 : Integer.parseInt(record.get("numRatings")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setLikedPercent(record.get("likedPercent").isEmpty() ? 0f : Float.parseFloat(record.get("likedPercent")));
                } catch (NumberFormatException nfe) {
                    logger.warn("Parsing {} for {}", nfe.getMessage(), book.getTitle());
                }

                try {
                    book.setPublishDate(record.get("publishDate").isEmpty() ? null : sdf.parse(record.get("publishDate")));
                } catch (ParseException pe) {
                    logger.warn("{} for {}", pe.getMessage(), book.getTitle());
                }

                try {
                    book.setFirstPublishDate(record.get("firstPublishDate").isEmpty() ? null : sdf.parse(record.get("firstPublishDate")));
                } catch (ParseException pe) {
                    logger.warn("{} for {}", pe.getMessage(), book.getTitle());
                }

                try {
                    int[] stars = Arrays.stream(objectMapper.readValue(record.get("ratingsByStars").replaceAll("'", "\""), String[].class)).mapToInt(Integer::parseInt).toArray();
                    book.setStars(new Stars(stars));
                } catch (IllegalArgumentException iae) {
                    logger.warn("{} for {}", iae.getMessage(), book.getTitle());
                }

                String[] rawGenres = objectMapper.readValue(record.get("genres").replaceAll("'", "\""), String[].class);
                List<Genre> genres = Arrays.stream(rawGenres).map(name -> {
                    if (genreRepo.existsGenreByName(name)) {
                        return genreRepo.findFirstByName(name);
                    } else {
                        Genre genre = new Genre();
                        genre.setName(name);
                        return genreRepo.save(genre);
                    }
                }).toList();
                book.setGenre(genres);
                Book savedBook = bookRepo.save(book);

                String[] rawAuthors = record.get("author").split(",");
                List<AuthorContribution> authors = Arrays.stream(rawAuthors).map(authorText -> {
                    Matcher matcher = authorPattern.matcher(authorText.trim());
                    if (!matcher.matches()) {
                        return null;
                    }

                    Author author = new Author();
                    author.setName(matcher.group("name"));

                    if (authorRepo.existsAuthorByName(matcher.group("name"))) {
                        author = authorRepo.findFirstByName(matcher.group("name"));
                    } else {
                        author = authorRepo.save(author);
                    }

                    return new AuthorContribution(savedBook, author, matcher.group("contrib"));
                }).toList();

                authorContribRepo.saveAllAndFlush(authors);

                count++;
            }

            return count;
        } catch (IOException ioe) {
            logger.error("Failed to seed database", ioe);
        }

        return 0L;
    }
}
