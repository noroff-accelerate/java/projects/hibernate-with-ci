package no.qux.demo.hibernate.repository;

import no.qux.demo.hibernate.model.dbo.Author;
import no.qux.demo.hibernate.model.dbo.AuthorContribution;
import no.qux.demo.hibernate.model.dbo.AuthorContributionId;
import no.qux.demo.hibernate.model.dbo.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorContributionRepository extends JpaRepository<AuthorContribution, AuthorContributionId> {
    List<AuthorContribution> findAllByAuthor (Author author);
    List<AuthorContribution> findAllByBook (Book book);
}
