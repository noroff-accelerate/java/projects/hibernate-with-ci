package no.qux.demo.hibernate.repository;

import no.qux.demo.hibernate.model.dbo.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
