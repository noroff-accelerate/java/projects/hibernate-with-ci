package no.qux.demo.hibernate.repository;

import no.qux.demo.hibernate.model.dbo.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
    boolean existsGenreByName(String name);
    Genre findFirstByName(String name);
}
