package no.qux.demo.hibernate.repository;

import no.qux.demo.hibernate.model.dbo.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
    boolean existsAuthorByName(String name);
    Author findFirstByName(String name);
}
