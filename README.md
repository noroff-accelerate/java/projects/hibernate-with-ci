# Hibernate Demo on Heroku

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://accelerate-hibernate-demo.herokuapp.com/swagger-ui/index.html)
[![container](https://img.shields.io/static/v1?logo=docker&message=Registry&label=Docker&color=2496ED)](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci/container_registry)
<!-- [![pipeline status](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci/badges/master/pipeline.svg)](https://noroff-accelerate.gitlab.io/java/projects/hibernate-with-ci/tests) -->
<!-- [![coverage report](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci/badges/master/coverage.svg)](https://noroff-accelerate.gitlab.io/java/projects/hibernate-with-ci/coverage) -->
<!-- [![codecov](https://codecov.io/gl/noroff-accelerate:java:projects/hibernate-with-ci/branch/master/graph/badge.svg?token=F6DZDTNS86)](https://codecov.io/gl/noroff-accelerate:java:projects/hibernate-with-ci) -->

Demo deployment of a Spring application to Heroku

## Table of Contents

- [Background](#background)
  - [Deploying Postgres](#deploying-postgres)
  - [Heroku Deployment Considerations](#heroku-deployment-considerations)
  - [Disabling Spring Security](#disabling-spring-security)
  - [OAuth2 Resource Server & Keycloak Considerations](#oauth2-resource-server-and-keycloak-considerations)
  - [CORS](#cors)
  - [Setting up a Keycloak Service](#setting-up-a-keycloak-service)
  - [CI Setup](#ci-setup)
  - [Additional Libraries and Considerations](#additional-libraries-and-considerations)
  - [A Primer on URIs](#a-primer-on-uris)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Hibernate is an Object Relational Model library that provides an easy and standardized way to write Java Spring applications and interact with a variety of databases, and across multiple dialects of SQL. This demo primarily demonstrates the use of Hibernate to build a simple REST API that is secured using Spring Security.

This demo requires several external services:

- [x] Postgres is a mature and extensible SQL database that is supported by pretty much everything and isn't tainted by Microsoft. Google it. 
- [x] Keycloak is an easily deployed identity provider, maintained by Red Hat and designed for ease of developing applications applications secured using one of many SSO authentication mechanisms such as OAuth2, OpenID Connect, or SAML2.

### Deploying Postgres

Postgres is deployed using Docker, using the Docker Compose configuration `docker-compose.yml`. This is setup to accept two environment variables:

- `POSTGRES_DB` &mdash; the name of the postgres database
- `POSTGRES_PASSWORD` &mdash; the only environment variable that is strictly required by the Postgres image.

These are specified in a file that you must create called `.env` (without file extension). This is done by copying `.env.example` and modify values to your preference.

You can then start the Postgres server by running the following command:

```shell
docker-compose up -d postgres
```

### Heroku Deployment Considerations

When deployed with Heroku, the Heroku addon that supplies a Postgres database to the running application uses a different URI format to that expected by the JDBC driver.

To correct for this behaviour, several changes need to be made to the application:

#### Translating the DATABASE_URL

Heroku will supply the credentials to your application via an environment variable named `DATABASE_URL`. These credentials will require translation, as they are supplied in the following initial format:

```
postgres://user:password@host:port/dbname
```

This differs significantly from the expected format:

```
jdbc:postgresql://host:port/dbname?sslmode=require&user=user&password=supersecretpassword
```

The suggested method for translation is discussed in the [official Heroku documentation](https://devcenter.heroku.com/articles/connecting-to-relational-databases-on-heroku-with-java#using-the-database_url-in-spring-with-java-configuration), however the final method for handling the translation is the following:

```java
@Configuration
@Profile("production")
public class DataSourceConfig {
    @Value("#{systemEnvironment['DATABASE_URL']}")
    String databaseUrl;

    @Bean
    public BasicDataSource dataSource() throws URISyntaxException {
        URI databaseUri = new URI(databaseUrl);
        String[] creds = databaseUri.getUserInfo().split(":");
        String url = String.format("jdbc:postgresql://%s:%s%s?sslmode=require&user=%s&password=%s",
                databaseUri.getHost(),
                databaseUri.getPort(),
                databaseUri.getPath(),
                creds[0],
                creds[1]);

        BasicDataSource source = new BasicDataSource();
        source.setUrl(url);

        return source;
    }
}
```

#### Additional Dependency

The inclusion of the above configuration requires an additional dependency be added to `build.gradle`:

```
implementation 'org.apache.commons:commons-dbcp2:2.+'
```

#### Production vs. Development

Including the above snippet in your application will not produce any meaningful change in behaviour during development due to the following annotation:

```java
@Profile("production")
```

This indicates to Spring that the class should only be registered on the dependency injector if the `production` profile is active. For this behaviour to take effect, we must specify which profiles are active when we deploy our application. To accomplish this we modify our `application.properties`:

```
# Allow active profile to be set by env variable. If none specified then activate 'development'.
spring.profiles.active=${SPRING_PROFILE:development}
```

Additionally, to ensure that our application runs in production mode by default when running inside a Docker container, we override the default value of the environment variable inside our `Dockerfile`:

```dockerfile
# Default active profile should be 'production' if not otherwise spectified
ENV SPRING_PROFILE production
```

The above can be omitted, but will then require that you override the `SPRING_PROFILE` config variable on Heroku's dashboard.

Although we are setting the default behaviour to enable the production profile in our Docker image, there are also times where we are running a Docker container but don't want to translate the `DATABASE_URL`; such as when we are running the application locally using Docker Compose. To account for this, we specify the `SPRING_PROFILE` environment variable inside our `docker-compose.yml` as well:

```yaml
version: "3"

services:
  web:
    build: .
    environment:
      # Provide DATABASE_URL in the correct format
      DATABASE_URL: jdbc:postgresql://postgres:5432/${POSTGRES_DB}?sslmode=prefer&user=postgres&password=${POSTGRES_PASSWORD}
      # Override active profile so that DataSourceConfig does not get registered on the dependency injector
      SPRING_PROFILE: development
    ports:
      - "8080:8080"
  
  # ...
```

### Disabling Spring Security

When generating a new application that includes Spring Security, the default configuration locks the entire application behind a basic login page. If this is acceptable then no further configuration is necessary, however this behaviour can be disabled.

The default username to get past the login screen is `user` and a random password is generated and logged to _stdout_ on each run of the application. More complex authentication schemes will be elaborated on later.

To disable this behaviour, or to return the application to the typical behaviour of an app _without_ spring security installed, the following class needs to be added to your project:

```java
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // Enable CORS -- this is further configured on the controllers
                .cors().and()

                // Sessions will not be used
                .sessionManagement().disable()

                // Disable CSRF -- not necessary when there are no sessions
                .csrf().disable()
                
                // Disable authorization checks for all requests
                .authorizeHttpRequests(authorize -> authorize.anyRequest().permitAll());
    }
}
```

### OAuth2 Resource Server and Keycloak Considerations

The primary function to secure a REST API service is the ability to authenticate, consume, and make access control decisions based on a supplied JWT. Spring provides tools to achieve this with minimal configuration.

#### Retrieving a JWT

How the JWT is supplied to the application is most often irrelevant as this is typically handled by an external application such as an Authentication Gateway. Some examples of this include:

- Nginx and [OAuth2 Proxy](https://oauth2-proxy.github.io/oauth2-proxy/)
- Various SaaS offerings from major cloud providers
  - [Oracle](https://docs.oracle.com/en/solutions/extend-saas-cloud-native/create-api-gateway-deployments1.html#GUID-EE0A5A2B-4634-4CA2-90B2-A9053ECD9322)
  - [Azure AD](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-protect-backend-with-aad)
  - [AWS API Gateway](https://aws.amazon.com/premiumsupport/knowledge-center/cognito-custom-scopes-api-gateway/)
  - etc.

For testing however, the above solutions are not pragmatic. For testing and demonstration, we will be retrieving our JWTs from our IdP using the OAuth2 Implicit Flow from our frontend applications. This is [not recommended for production use](https://auth0.com/docs/get-started/authentication-and-authorization-flow/which-oauth-2-0-flow-should-i-use#is-the-client-a-single-page-app-) as one typically does not want to expose the access token to the client application, however using this approach saves us much additional setup that is not necessary for our testing.

#### Client Option 1: OpenAPI

The first option for retrieving a JWT for testing is to use the built-in mechanism for OpenAPI; this is also necessary to make the API documentation work once access controls are enabled on the service.

First, an additional dependency is required:

```
// Should already exist
implementation 'org.springdoc:springdoc-openapi-ui:1.+'
// Add this below
implementation 'org.springdoc:springdoc-openapi-security:1.+'
```

Next, you need to add the `@SecurityScheme` annotation to your configuration class for OpenAPI. If it doesn't already exist then you can create it.

```java
@OpenAPIDefinition(info = @Info(title = "Accelerate Hibernate Demo", description = "Hibernate with CI and Spring Security", version = "0.1.0"))
@SecurityScheme(name = "keycloak_implicit", 
        type = SecuritySchemeType.OAUTH2,
        flows = @OAuthFlows(authorizationCode = @OAuthFlow(
                authorizationUrl = "${spring.security.oauth2.resourceserver.jwt.issuer-uri}/protocol/openid-connect/auth", 
                tokenUrl = "${spring.security.oauth2.resourceserver.jwt.issuer-uri}/protocol/openid-connect/token", 
                scopes = {
                        @OAuthScope(name = "openid", description = "OpenID Connect Endpoints"),
                        @OAuthScope(name = "profile", description = "Standard profile claims excluding phone and email"),
                        @OAuthScope(name = "email", description = "Include email address information in profile"),
                        @OAuthScope(name = "groups", description = "Group membership information"),
                }
        ))
)
public class OpenApi3Config {}
```

Here we specify important information for how we expected to direct the user to authenticate and return with a valid JWT. We must to provide the `authorizationUrl` and `tokenUrl`, which are both able to be derived in a predicatable way from the `issuer-uri` (see [IdP discovery document](#openid-connect-discovery-document) for more details). The scopes specified using the `@OAuthScope` annotations are necessary as they form part of the authentication options the user can choose when authorizing with the Swagger UI.

The URLs of the above configuration are specified in terms of the `issuer-uri` which must be supplied as part of the `application.properties` file. Some additional options are also specified, generally with the option to be overriden as environment variables:

```
springdoc.swagger-ui.oauth.client-id=${CLIENT_ID:client-id}
springdoc.swagger-ui.oauth.client-secret=${CLIENT_SECRET:client-secret}
springdoc.swagger-ui.oauth.use-pkce-with-authorization-code-grant=true
spring.security.oauth2.resourceserver.jwt.issuer-uri=${ISSUER_URL:http://localhost:8083/auth/realms/myrealm}
```

The values for `CLIENT_ID`, `CLIENT_SECRET` (where applicable) and `ISSUER_URL` are acquired from [setting up a keycloak service](#setting-up-a-keycloak-service), and also will require some additional dependencies in our project to be added to `build.gradle`:

```
// Should already have this
implementation 'org.springframework.boot:spring-boot-starter-security'
        
// Add these
implementation 'org.springframework.security:spring-security-oauth2-resource-server'
implementation 'org.springframework.security:spring-security-oauth2-jose'
```

#### Client Option 2: Keycloak.js

> Note: you should only integrate your frontend application once you are successfully able to test your authentication and access control from your Swagger UI in [Option 1](#client-option-1-openapi).

> TODO

#### Enable OAuth2 Resource Server

Reconfigure your security configuration class to only permit anonymous access to requests on the paths related to the OpenAPI documentation and Swagger UI. All other requests are marked as requiring authentication. Spring Security handles the remaining details for this and will automatically respond appropriately (i.e. `401 Unauthorized`) to requests that do not contain valid credentials.

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
            // Enable CORS -- this is further configured on the controllers
            .cors().and()

            // Sessions will not be used
            .sessionManagement().disable()

            // Disable CSRF -- not necessary when there are sessions
            .csrf().disable()

            // Enable security for http requests
            .authorizeRequests(authorize -> {
                authorize
                        // Specify paths where public access is allowed
                        .antMatchers("/.well-known/oas/**").permitAll()
                        .antMatchers("/swagger-ui/**").permitAll()

                        // All remaining paths require authentication
                        .anyRequest().authenticated();
            })
        
            // ...
        
```

The remaining configuration enables JWT handling and specifies how claims should be converted into Spring Security `GrantedAuthorities`. These are used to enact access control on the resources you define in your controllers.

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
        
            // ...
        
            // Configure OAuth2 Resource Server (JWT authentication)
            .oauth2ResourceServer(oauth2 -> {
                // Convert Jwt to AbstractAuthenticationToken
                JwtAuthenticationConverter authnConverter = new JwtAuthenticationConverter();

                // Convert Jwt roles claim to GrantedAuthorities
                JwtGrantedAuthoritiesConverter roleConverter = new JwtGrantedAuthoritiesConverter();
                roleConverter.setAuthorityPrefix("ROLE_");
                roleConverter.setAuthoritiesClaimName("roles");

                // Jwt -> GrantedAuthorities -> AbstractAuthenticationToken
                authnConverter.setJwtGrantedAuthoritiesConverter(roleConverter);

                // Enable JWT authentication and access control from JWT claims
                oauth2.jwt().jwtAuthenticationConverter(authnConverter);
            });
}
```

#### Securing an Endpoint

We can secure our controllers using annotations.

First, we can tell our OpenAPI documentation that a controller requires authentication using `@SecurityRequirement`:

```java
@RestController
@SecurityRequirement(name = "keycloak_implicit") // Should match our OpenAPI config
public class RootAPIController {
    // ...
}
```

Then we can tell our endpoint to require a particular `GrantedAuthority` using `@PreAuthorize` and `hasAuthority`:

```java
@GetMapping("/")
@PreAuthorize("hasAuthority('ROLE_admin')")
public ResponseEntity<CommonResponse<String>> hello() {
    return ResponseEntity
            .ok(new CommonResponse<>("Hello!"));
}
```

Instead of `hasAuthority` we can also use `hasRole`, which allows us to omit the `ROLE_` prefix:

```java
@GetMapping("/")
@PreAuthorize("hasRole('admin')")
public ResponseEntity<CommonResponse<String>> hello() {
    return ResponseEntity
            .ok(new CommonResponse<>("Hello!"));
}
```

We can also inspect the JWT contents ourselves by injecting it into the parameters:

```java
@GetMapping("/inspect")
public ResponseEntity<CommonResponse<Object>> inspect(
        @AuthenticationPrincipal Jwt principal
) {
    if (principal.getClaims().get("isSuperAdmin").equals("no")) {
        return ResponseEntity.forbidden(new CommonResponse<>(403, "Only cool people can inspect tokens"));
    }
    
    return ResponseEntity
            .ok(new CommonResponse<>(principal.getClaims().get("sub")));
}
```

### CORS

When deploying the frontend and backend applications to separate web addresses, by default the backend application will not accept requests from origins other than its own. As we enabled CORS-checking in our `SecurityConfiguration` class, we must explicitly allow any other origin we want to be able to use our API. This is primarily done using the `@CrossOrigin` annotation on our controller classes or endpoints.

```java
@RestController
@CrossOrigin("http://localhost:3000")
public class RootAPIController {}
```

The annotation above will allow requests against our API from a specific origin and should be included on each controller or endpoint for which that origin is relevant. You can also add additional origins by separating them with a comma:

```java
@RestController
@CrossOrigin(
        origins = { 
                "https://username.gitlab.io", 
                "http://localhost:3000" 
        }
)
public class RootAPIController {}
```

Specific headers and methods may also be configured:

```java
@RestController
public class RootAPIController {

    @CrossOrigin(
            origins = { "http://localhost:3000" },
            methods = RequestMethod.GET,
            headers = "X-MY-SECRET-HEADER"
    )
    @GetMapping("/")
    public ResponseEntity<CommonResponse<String>> hello() {
        return ResponseEntity
                .ok(new CommonResponse<>("Hello!"));
    }
}
```

Additionally, as allowed origins are likely to be reused across many endpoints, we can also supply a configuration variable to the annotation in place of a static string:

```java
@RestController
@CrossOrigin("${server.cors.application_origin}")
public class RootAPIController {}
```

The above snippet would allow us to specify a value from our `application.properties` file, which we can specify as follows:

```
server.cors.application_origin=${APP_ORIGIN:http://localhost:3000}
```

This would allow us to override the value as an environment variable at runtime, while providing `http://localhost:3000` as a reasonable default for development.

For more information please see a [more detailed guide on CORS in Spring Security](https://www.stackhawk.com/blog/spring-cors-guide).

### Setting up a Keycloak Service

Setting up a local keycloak service is most simply done using Docker Compose. Add the following services to your `docker-compose.yml` file:

```yaml
version: "3"

services:
  # ...

  keycloak-db:
    image: postgres:14-alpine
    restart: always
    environment:
      POSTGRES_PASSWORD: keycloak
      POSTGRES_USER: keycloak
      POSTGRES_DB: keycloak
      PGDATA: /var/lib/postgresql/data/pgdata
    volumes:
      - keycloakdata:/var/lib/postgresql/data/pgdata
  
  keycloak:
    image: jboss/keycloak
    restart: always
    environment:
      KEYCLOAK_USER: ${KEYCLOAK_USER:admin}
      KEYCLOAK_PASSWORD: ${KEYCLOAK_PASSWORD:secret}
      DB_VENDOR: postgres
      DB_USER: keycloak
      DB_PASSWORD: keycloak
      DB_ADDR: keycloak-db
    ports:
      - '127.0.0.1:8083:8080'

volumes:
  # ...
  keycloakdata: {}
```

This configuration defines a separate instance of PostgreSQL for Keycloak to store its configuration, and the keycloak instance itself, which will serve on http://localhost:8083. Documentation for the Keycloak Docker image and its configuration options can be found [here](https://hub.docker.com/r/jboss/keycloak/).

You can also optionally create a `.env` file to contain secrets that _**SHOULD NOT**_ be committed to your Git repositories:

```
KEYCLOAK_USER=admin
KEYCLOAK_PASSWORD=secret
```

Once your changes are made, you can simply re-up your Docker compose without killing your existing containers by running:

```bash
docker-compose up -d
```

#### Resetting Keycloak

If you ever need to reset your instance of keycloak (i.e. if you forgot to specify the admin password and it generated a random one for you) then you should do the following:

```bash
# Stop your running instances
docker-compose down

# Find the appropriate volume for the keycloak data and note its name
docker volume ls

# Delete the volume; this will delete any existing configuration
docker volume rm volume-name

# Restart containers with freshly generated configuration
docker-compose up -d
```

#### Configuring Keycloak

Once successfully logged into keycloak using the credentials supplied earlier, you can begin configuring your realm.

**Realm.** First check that you are on the correct realm. If you are currently looking at the "Master" realm then you need to create a new realm. Choose a short name that does not contain upper case letters or spaces, like `demo` or `myrealm`. This also determines the URL of your `issuer`, which will be:

```
http://localhost:8083/auth/realms/realm-name
http://localhost:8083/auth/realms/demo
http://localhost:8083/auth/realms/myrealm
```

Once created, you should be able to resolve your [discovery document](#openid-connect-discovery-document).

**Client.** Next a client application is required inside keycloak. When creating the client, specify a unique `CLIENT_ID` and note it down for use in configuration. Ensure that Client Protocol is set to `openid-connect` and create the client.

In the client configuration screen, ensure the following:

1. Client is enabled.
2. Client access type is `public` or `confidential`. For use with implicit flow, `public` is sufficient. Keycloak will not expose a client secret unless the client type is set to `confidential`.
3. Implicit Flow is enabled.
4. Valid Redirect URIs contains:
   1. The complete URL of your backend application when hosted locally, i.e. `http://localhost:8080/*`. You should remove this for production deployments.
   2. The complete URL of your backend application when hosted remotely, i.e. `https://my-app.herokuapp.com/*`. **NOTE THE "S" IN HTTPS**.
   3. The complete URL of your frontend application when hosted locally, i.e. `http://localhost:3000/*`. You should remove this for production deployments.
   4. The complete URL of your frontend application when hosted remotely, i.e. `https://username.gitlab.io/my-awesome-app/*`. **NOTE THE "S" IN HTTPS**.
5. Web Origins contains:
   1. `+` &mdash; this is a special wildcard that specifies all valid origins from the Valid Redirect URIs.
6. All other fields should remain as their defaults.

**Mappers.** Mappers are required to modify the contents of your JWTs to include things like roles and groups to be used for access control. Instructions for how to do this are not covered here and can be found elsewhere.

Once you supply your `ISSUER_URL` and `CLIENT_ID` to your application, your authentication flow should work as intended.

#### OpenID Connect Discovery Document

Any OpenID Connect compliant IdP publishes a "discovery document" in a predictable location that can be used to automatically configure resource servers with all pertinent information, including the ability to register new clients dynamically if that functionality is enabled.

The discovery document for any compliant IdP can be found relative to the location of the `issuer`, as it appears in the `iss` claim of resulting JWTs.

```
# Append this path to your issuer
/.well-known/openid-configuration
```

If the `issuer` is `http://localhost:8083/auth/realms/myrealm`, then the discovery document will be located at:

```
http://localhost:8083/auth/realms/myrealm/.well-known/openid-configuration
```

All other URLs are not fixed but are instead resolved from the resulting JSON document that is available at that location. For example, the various URLs used in the authorization for Keycloak are as follows:

```json
{
  "issuer": "http://localhost:8083/auth/realms/myrealm",
  "authorization_endpoint": "http://localhost:8083/auth/realms/myrealm/protocol/openid-connect/auth",
  "token_endpoint": "http://localhost:8083/auth/realms/myrealm/protocol/openid-connect/token",
  "introspection_endpoint": "http://localhost:8083/auth/realms/myrealm/protocol/openid-connect/token/introspect",
  "userinfo_endpoint": "http://localhost:8083/auth/realms/myrealm/protocol/openid-connect/userinfo",
  "jwks_uri": "http://localhost:8083/auth/realms/myrealm/protocol/openid-connect/certs",
  "grant_types_supported": ["authorization_code","implicit","refresh_token","password","client_credentials", ...],
  "response_types_supported": ["code","none","id_token","token","id_token token","code id_token","code token","code id_token token"],
  "subject_types_supported": ["public","pairwise"],
  // ...
}
```

The location of the discovery document does not change so that clients can acquire the document without prior configuration. Additionally, the field names and format of contents are also persistent. For example, the `authorization_endpoint` field will always exist and contain a string URI that points to the endpoint that handles authorization requests.

### CI Setup

As with the previous demo, this repository shows a Spring application with a complete CI setup to test, build, and deploy the final application on Heroku.

The CI pipeline will:

- ~~Run tests, proceeding if the tests pass. Test coverage information is uploaded to Codecov, a test coverage tracker and explorer.~~ 

> This step is currently disabled as I was unable to figure out how to tell Hibernate to not connect the database in testing mode. Testing in this repo remains a TODO for future iterations. PRs and suggestions are welcome; see [issue](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci/-/issues/1) for details.

- Build the project into a production artifact, proceeding if successful.
- Package the built application into a Docker image, proceeding if successful.
- Push the Docker image to the local registry on Gitlab. This requires a manual trigger.
- Trigger the build pipeline on Heroku to pull the latest image and replace the current running dyno.

### Additional Libraries and Considerations

Some additional considerations are addressed with the addition of additional libraries.

#### [ModelMapper](http://modelmapper.org/getting-started/)

Projecting database objects (DBOs) to data transfer objects (DTOs) can be cumbersome, however there exists the ModelMapper library which analyzes two types and creates a mapping between them with a single function call. In that automatic fails, a manual mapping may be created to maintain the core use of the library across your application.

#### [JNanoId](https://github.com/aventrix/jnanoid)

A Java port of the popular NanoId Javascript library. This tiny library is a secure, URL-friendly, unique string ID generator for use in databases and APIs that are less cumbersome than UUIDs (21 characters vs. 36) and do not leak intelligence like auto-incrementing integers.

### A Primer on URIs

URIs take the following general form:

```
scheme://username:password@target/path/to/your/resource?query=data&foo=bar#fragment
```

Where `target` can be something like a network address of the form: `host[:port]`. For example:

- `foo.example.com`
- `hello.herokuapp.com`
- `localhost:8080`

The REST API endpoints for the `CustomerContoller` look something like this:

```
GET http://localhost:8080/api/customer
GET http://localhost:8080/api/customer/:id
POST http://localhost:8080/api/customer
PUT http://localhost:8080/api/customer/:id
PATCH http://localhost:8080/api/customer/:id
DELETE http://localhost:8080/api/customer/:id
```

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

For Linux/macOS users, open a terminal and run:

```sh
./gradlew bootRun
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

## Acknowledgements

- `books.csv` ([DOI](https://doi.org/10.5281/zenodo.4265096)) &mdash; The author is grateful for the use of a freely available database of Books from the Goodreads API by  Lorena Casanova Lozano and Sergio Costa Planells.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Noroff Accelerate AS
